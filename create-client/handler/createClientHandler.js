const { commandMapper } = require("ebased/handler");

const inputMode = require("ebased/handler/input/commandApi");
const outMode = require("ebased/handler/output/commandApi");

const createClientDomain = require("../domain/createClientDomain");

module.exports.handler = async (command, context) => {
  return commandMapper(
    { command, context },
    inputMode,
    createClientDomain,
    outMode
  );
};
