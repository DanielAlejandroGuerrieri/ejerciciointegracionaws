const { createClientSchema } = require("../schemas/createClientSchema");
const { sendCreateClient } = require("../services/dynamoDbClient");
const { sendCreateClientSNS } = require("../services/snsCreateClient");
const { clientCreatedSchema } = require("../schemas/clientCreatedSchema");

module.exports = async (commandPayload, commandMeta) => {
  new createClientSchema(commandPayload, commandMeta);
  await sendCreateClient(commandPayload);
  await sendCreateClientSNS(
    new clientCreatedSchema(commandPayload, commandMeta)
  );

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "Success"
    }),
  };
};
