const sns = require("ebased/service/downstream/sns");

const sendCreateClientSNS = async (clientCreateEvent) => {
  const { eventPayload, eventMeta } = clientCreateEvent.get();

  await sns.publish(
    {
      TopicArn: process.env.CLIENTS_CREATED_TOPIC,
      Message: eventPayload,
    },
    eventMeta
  );
};

module.exports = { sendCreateClientSNS };