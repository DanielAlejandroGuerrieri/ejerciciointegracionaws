const dynamodb = require("ebased/service/storage/dynamo");

const sendCreateClient = async (payload) => {
  await dynamodb.putItem({
    TableName: process.env.CLIENTS_TABLE,
    Item: payload,
  });
};

module.exports = { sendCreateClient };