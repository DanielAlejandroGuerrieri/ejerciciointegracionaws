const { InputValidation } = require("ebased/schema/inputValidation");

class createClientSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.source,
      payload,
      type: "CLIENT.LAMBDA",
      specversion: "1.0.0",
      schema: {
        strict: true,
        dni: { type: String, required: true },
        name: { type: String, required: true },
        lastName: { type: String, required: true },
        birth: { type: String, required: true },
      },
    });
  }
}

module.exports = {
  createClientSchema,
};