const { DownstreamEvent } = require("ebased/schema/downstreamEvent");

class clientCreatedSchema extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      meta,
      payload,
      type: "CLIENT.LAMBDA",
      specversion: "1.0.0",
      schema: {
        strict: true,
        dni: { type: String, required: true },
        name: { type: String, required: true },
        lastName: { type: String, required: true },
        birth: { type: String, required: true },
      },
    });
  }
}

module.exports = { clientCreatedSchema };